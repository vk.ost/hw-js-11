"use strict"

const passwordInputs = document.querySelectorAll(".password-input");
const iconsPassword = document.querySelectorAll(".icon-password");
iconsPassword.forEach((elem)=>{
    elem.addEventListener("click", changIcon);
});
function changIcon(event) {
    if(event.target.classList.toggle("fa-eye-slash")){
        event.target.parentElement.querySelector("input").setAttribute('type', 'text');
    }else{
        event.target.parentElement.querySelector("input").setAttribute('type', 'password');
    }
}


const btn = document.querySelector(".btn");

btn.addEventListener('click', (event)=>{
    event.preventDefault();
    if(passwordInputs[0].value.trim() === "" || passwordInputs[1].value.trim() === ""){
        return alert('Нужно ввести значения');
    }
    if(passwordInputs[0].value === passwordInputs[1].value){
        alert("You are welcome");
    }else{
        alert('Нужно ввести одинаковые значения');
    }
});

